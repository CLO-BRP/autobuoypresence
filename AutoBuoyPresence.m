% Script AutoBuoyPresence
%
% Report number of hours per week by site

% Initializations
clear
import utilities.*
infileFull = fullfile('.','data','input',...
    'AB_Historical_Data_2008-2018_DailyPresence_DataGap_MSP2.xlsx');
outfile = sprintf('Presence_%s.xlsx',datestr(now,'yyyymmdd_HHMMSS'));
outpath = fileparts(mfilename('fullpath'));
outpath = fullfile(outpath,'data','output');
if ~isdir(outpath)
    mkdir(outpath)
end
outpathFull = fullfile(outpath,outfile);


% Read in autobuoy workbook
[~,~,C] = xlsread(infileFull);
dt = datenum(C(2:end,1));
site = C(1,2:end);
M = cell2mat(C(2:end,2:end));

%---
% Plot: Cumulative Days per Week for each site
%% ---
firstDay = datetime(min(dt),'ConvertFrom','datenum');
lastDay = datetime(max(dt),'ConvertFrom','datenum');
numDays = caldays(between(firstDay,lastDay,'days')) + 1;
numWeeks = ceil(numDays/7);
datesWeekly =  firstDay + calweeks(0:numWeeks-1);
numSites = length(site);

% Find weekly presence
daysPerWeek = NaN(numWeeks,numSites);
effortPerWeek = zeros(numWeeks,numSites);
inc = 7; % days per week;
for i = 1:numWeeks
    currMax = min(i*inc,numDays);
    currWeek = nansum(M((i-1)*inc+1:currMax,:),1);
    currWeek(all(isnan(M((i-1)*inc+1:currMax,:)),1)) = NaN;
    daysPerWeek(i,:) = currWeek;
    currEffort = sum(~isnan(M((i-1)*inc+1:currMax,:)),1);
    effortPerWeek(i,:) = currEffort;
end

screenSize = get(0,'ScreenSize');
shim = 35 / screenSize(4);
ax = NaN(numSites,1);
fh = figure('Color',[1,1,1],'Position',screenSize);
setappdata(fh, 'SubplotDefaultAxesLocation', [0.075,0.0375,0.8375,0.9])
grey = [0.9,0.9,0.9];
grey2 = [0.6,0.6,0.6];
xlim = [datesWeekly(1)-hours(3.5*24), datesWeekly(end)+hours(3.5*24)];
xtick = firstDay + calyears(0:numWeeks/26);
ylim = [0 max(max(daysPerWeek))*1.15];

for i = 1:numSites
    
    % Plot number of hour per week presence
    currAx = subplot(numSites,1,i);
    yyaxis left
% %     bar(currAx,datesWeekly,daysPerWeek(:,i),1,'k','EdgeColor','w')
    bar(currAx,datesWeekly,daysPerWeek(:,i),1,'k')
    hold on;
    currAx.YLabel.String = sprintf('Site %s\n',site{i});
    xtickformat('yyyy')
    currAx.XLim = xlim;
    currAx.XTick = xtick;
    if ~isequal(i,numSites)
        currAx.XTickLabel = [];
    end
    currAx.Position(4) = currAx.Position(4) + shim;
    currAx.YLim = ylim;
    currAx.YColor = 'k';
 
    % Plot recording effort
    currEffort = effortPerWeek(:,i);
    noEffort = 100 .* (currEffort == 0);
    yyaxis right
% %     bar(currAx,datesWeekly,noEffort,1,'FaceColor',grey,'EdgeColor','w')
    bar(currAx,datesWeekly,noEffort,1,'FaceColor',grey)
    currAx.YLabel.String = sprintf('Percent of\nEffort per Week');
    currAx.XLim = xlim;
    currAx.YLim = [0,100];
    currAx.YColor = 'k';
        
    % Plot proportion of effort for each week, if less that 1
    currEffort2 = 100 .* currEffort ./ inc;
    currEffort2(currEffort2==0 | currEffort2==100) = NaN;
    plot(currAx,datesWeekly,currEffort2,'o',...
        'MarkerSize',3,'MarkerFaceColor',grey2,'MarkerEdgeColor',grey2);
    currAx.YLim = [0,100];
    ax(i) = currAx;
    hold off;
end
mtit(fh,sprintf('PRESENCE\n(Site-days per Week)'),'xoff',0,'yoff',0.01);
outfileName = fullfile(outpath,...
    sprintf('Presence_SiteDaysPerWeek_%s',datestr(now,'yyyymmdd_HHMMSS')));
saveas(fh,outfileName);

%---
% Tabulate: Cumulative Days per Week for each site
%% ---
%---
% Cumulative Days Per Week - Presence
%---

% Initializations
sz = size(M);
uniqueSite = unique(site);

% Add header
C = cell(numWeeks+8, sz(2)+2);
C(1,1) = {'CUMULATIVE DAYS PER WEEK'};
C(3,1) = {['Worksheet Created: ',datestr(now)]};
C(5,2) = {'PRESENCE'};

% Add channel column headers
C(6,2:end-1) = uniqueSite;
chanStr = [sprintf('ch %.0f,',(1:numSites)), 'SUM'];
C(7,1) = {'Week'};
C(7,2:end) = strsplit(chanStr(1:end),',');

% Add date and time column
C(8:numWeeks+7, 1) = cellstr(datestr(firstDay:7:lastDay,'mm/dd/yyyy'));

% Add body of table
C(8:numWeeks+7, 2:end-1) = num2cell(daysPerWeek);

% Add row and column sums
rowsum = nansum(daysPerWeek,2);
rowsum(all(isnan(daysPerWeek),2)) = NaN;
colsum = nansum(daysPerWeek,1);
colsum(sum(isnan(daysPerWeek),1)==numDays) = NaN;
C(8:numWeeks+7, end) = num2cell(rowsum);
C(8+numWeeks, :) = [{'SUM'},num2cell(colsum),sum(colsum)];

%---
% Cumulative Days Per Week - Coverage
%---

% Add header
C2 = cell(numWeeks+8, sz(2)+2);
C2(5,2) = {'COVERAGE'};

% Add channel column headers
C2(6,2:end-1) = uniqueSite;
chanStr = [sprintf('ch %.0f,',(1:numSites)), 'SUM'];
C2(7,1) = {'Week'};
C2(7,2:end) = strsplit(chanStr(1:end),',');

% Add date and time column
C2(8:numWeeks+7, 1) = cellstr(datestr(firstDay:7:lastDay,'mm/dd/yyyy'));

% Add body of table
C2(8:numWeeks+7, 2:end-1) = num2cell(effortPerWeek);

% Add row and column sums
rowsum = nansum(effortPerWeek,2);
rowsum(all(isnan(effortPerWeek),2)) = NaN;
colsum = nansum(effortPerWeek,1);
colsum(sum(isnan(effortPerWeek),1)==numDays) = NaN;
C2(8:numWeeks+7, end) = num2cell(rowsum);
C2(8+numWeeks, :) = [{'SUM'},num2cell(colsum),sum(colsum)];

%---
% Cumulative Days Per Week - Presence Proporation
%---

% Add header
C3 = cell(numWeeks+8, sz(2)+2);
C3(5,2) = {'PRESENCE PROPORTION'};

% Add channel column headers
C3(6,2:end-1) = uniqueSite;
chanStr = [sprintf('ch %.0f,',(1:numSites)), ''];
C3(7,1) = {'Week'};
C3(7,2:end) = strsplit(chanStr(1:end),',');

% Add date and time column
C3(8:numWeeks+7, 1) = cellstr(datestr(firstDay:7:lastDay,'mm/dd/yyyy'));

% Add body of table
C3(8:numWeeks+7, 2:end-1) = num2cell(daysPerWeek ./ effortPerWeek);

% Concatenate presence and coverage tables
C = [C,C2,C3];

% Write output
xlswrite(outpathFull,C,'DaysPerWeek');

%---
% Format worksheet
%---

% Open workbook
visible = false;
[Excel,Workbooks,Workbook] = excel.open(outpathFull,visible);

%delete unused first worksheets
sheetIDX = 1;
excel.deleteWorksheet(Excel,sheetIDX); %delete unused first worksheet

% Adjust column widths
[m,n] = size(C);
width = ones(1,n) .* 5;
width([1,numSites+3,numSites*2+5]) = 10;
sheetIDX = 1;
excel.colWidth(Excel,sheetIDX,width)

% Center cells

cellIDX = false(m,n);
cellIDX(6:m,1:n) = true;
excel.alignCell(Excel,sheetIDX,cellIDX,'center')

%Bold font
boldIDX = false(m,n);
boldIDX(1,1) = true;
boldIDX(5:7,1:n) = true;
excel.boldFont(Excel,sheetIDX,boldIDX);

% Blue font for sums
color = 'bright blue';
colorIDX = false(m,n);
colorIDX(7:end,2+numSites) = true;
colorIDX(7:end,4+numSites*2) = true;
colorIDX(end,:) = true;
excel.fontColor(Excel,sheetIDX,colorIDX,color);

% Save open worksheet
excel.close(Excel,Workbook)

%---
% Plot: Cumulative Days per Month for each site
%% ---
numSites = length(site);

% Make month vector
month = cellstr(datestr(firstDay:lastDay,'mmm yyyy'));
uniqueMonths = unique(month,'stable');
numMonths = length(uniqueMonths);
datesMonthly =  firstDay + calmonths(0:numMonths-1);

% Find monthly presence
daysPresencePerMonth = NaN(numMonths,numSites);
effortPerMonth = zeros(numMonths,numSites);
daysPerMonth = NaN(numMonths,numSites);

for i = 1:numMonths
    idx = strcmp(month,uniqueMonths{i});
    currMonth = nansum(M(idx,:),1);
    currMonth(all(isnan(M(idx,:)),1)) = NaN;
    daysPresencePerMonth(i,:) = currMonth;
    currEffort = sum(~isnan(M(idx,:)),1);
    effortPerMonth(i,:) = currEffort;
    dv = datevec(month{i},'mmm yyyy');
    daysPerMonth(i,:) = eomday(dv(1),dv(2)); % inc is days in current month
end

screenSize = get(0,'ScreenSize');
shim = 35 / screenSize(4);
ax = NaN(numSites,1);
fh = figure('Color',[1,1,1],'Position',screenSize);
setappdata(fh, 'SubplotDefaultAxesLocation', [0.075,0.0375,0.8375,0.9])
grey = [0.9,0.9,0.9];
grey2 = [0.6,0.6,0.6];
xlim = [datesMonthly(1)-hours(3.5*24), datesMonthly(end)+hours(3.5*24)];
% % xtick = firstDay + calmonths(0:numWeeks/4.345);
% % xtick = firstDay:calyears(numMonths/12):lastDay;
xtick = firstDay + calyears(0:numMonths/12);
ylim = [0 max(max(daysPresencePerMonth))*1.15];

for i = 1:numSites
    
    % Plot number of hours per month presence
    currAx = subplot(numSites,1,i);
    yyaxis left
    bar(currAx,datesMonthly,daysPresencePerMonth(:,i),1,'k','EdgeColor','w')
    hold on;
    currAx.YLabel.String = sprintf('Site %s\n',site{i});
    xtickformat('yyyy')
    currAx.XLim = xlim;
    currAx.XTick = xtick;
    if ~isequal(i,numSites)
        currAx.XTickLabel = [];
    end
    currAx.Position(4) = currAx.Position(4) + shim;
    currAx.YLim = ylim;
    currAx.YColor = 'k';
 
    % Plot recording effort
    currEffort = effortPerMonth(:,i);
    noEffort = 100 .* (currEffort == 0);
    yyaxis right
    bar(currAx,datesMonthly,noEffort,1,'FaceColor',grey,'EdgeColor','w')
    currAx.YLabel.String = sprintf('Percent of\nEffort per Month');
    currAx.XLim = xlim;
    currAx.YLim = [0,100];
    currAx.YColor = 'k';
        
    % Plot proportion of effort for each month, if less that 1
    currEffort2 = 100 .* currEffort ./ daysPerMonth(:,i);
    currEffort2(currEffort2==0 | currEffort2==100) = NaN;
    plot(currAx,datesMonthly,currEffort2,'o',...
        'MarkerSize',3,'MarkerFaceColor',grey2,'MarkerEdgeColor',grey2);
    currAx.YLim = [0,100];
    ax(i) = currAx;
    hold off;
end
mtit(fh,sprintf('PRESENCE\n(Site-days per Month)'),'xoff',0,'yoff',0.01);
outfileName = fullfile(outpath,...
    sprintf('Presence_SiteDaysPerMonth_%s',datestr(now,'yyyymmdd_HHMMSS')));
saveas(fh,outfileName);

%---
% Tabulate: Cumulative Days per Month for each site
%% ---
%---
% Cumulative Days Per Month - Presence
%---

% Initializations
sz = size(M);
uniqueSite = unique(site);

% Add header
C = cell(numMonths+8, sz(2)+2);
C(1,1) = {'CUMULATIVE DAYS PER MONTH'};
C(3,1) = {['Worksheet Created: ',datestr(now)]};
C(5,2) = {'PRESENCE'};

% Add channel column headers
C(6,2:end-1) = uniqueSite;
chanStr = [sprintf('ch %.0f,',(1:numSites)), 'SUM'];
C(7,1) = {'Month'};
C(7,2:end) = strsplit(chanStr(1:end),',');

% Add date and time column
C(8:numMonths+7, 1) = uniqueMonths;

% Add body of table
C(8:numMonths+7, 2:end-1) = num2cell(daysPerMonth);

% Add row and column sums
rowsum = nansum(daysPerMonth,2);
rowsum(all(isnan(daysPerMonth),2)) = NaN;
colsum = nansum(daysPerMonth,1);
colsum(sum(isnan(daysPerMonth),1)==numDays) = NaN;
C(8:numMonths+7, end) = num2cell(rowsum);
C(8+numMonths, :) = [{'SUM'},num2cell(colsum),sum(colsum)];

%---
% Cumulative Days Per Month - Coverage
%---

% Add header
C2 = cell(numMonths+8, sz(2)+2);
C2(5,2) = {'COVERAGE'};

% Add channel column headers
C2(6,2:end-1) = uniqueSite;
chanStr = [sprintf('ch %.0f,',(1:numSites)), 'SUM'];
C2(7,1) = {'Month'};
C2(7,2:end) = strsplit(chanStr(1:end),',');

% Add date and time column
C2(8:numMonths+7, 1) = uniqueMonths;

% Add body of table
C2(8:numMonths+7, 2:end-1) = num2cell(effortPerMonth);

% Add row and column sums
rowsum = nansum(effortPerMonth,2);
rowsum(all(isnan(effortPerMonth),2)) = NaN;
colsum = nansum(effortPerMonth,1);
colsum(sum(isnan(effortPerMonth),1)==numDays) = NaN;
C2(8:numMonths+7, end) = num2cell(rowsum);
C2(8+numMonths, :) = [{'SUM'},num2cell(colsum),sum(colsum)];

%---
% Cumulative Days Per Month - Presence Proporation
%---

% Add header
C3 = cell(numMonths+8, sz(2)+2);
C3(5,2) = {'PRESENCE PROPORTION'};

% Add channel column headers
C3(6,2:end-1) = uniqueSite;
chanStr = [sprintf('ch %.0f,',(1:numSites)), ''];
C3(7,1) = {'Month'};
C3(7,2:end) = strsplit(chanStr(1:end),',');

% Add date and time column
C2(8:numMonths+7, 1) = uniqueMonths;

% Add body of table
C3(8:numMonths+7, 2:end-1) = num2cell(daysPerMonth ./ effortPerMonth);

% Concatenate presence and coverage tables
C = [C,C2,C3];

% Write output
xlswrite(outpathFull,C,'DaysPerMonth');

%---
% Format worksheet
%---

% Open workbook
visible = false;
[Excel,Workbooks,Workbook] = excel.open(outpathFull,visible);

% Adjust column widths
[m,n] = size(C);
width = ones(1,n) .* 5;
width([1,numSites+3,numSites*2+5]) = 10;
sheetIDX = 1;
excel.colWidth(Excel,sheetIDX,width)

% Center cells

cellIDX = false(m,n);
cellIDX(6:m,1:n) = true;
excel.alignCell(Excel,sheetIDX,cellIDX,'center')

%Bold font
boldIDX = false(m,n);
boldIDX(1,1) = true;
boldIDX(5:7,1:n) = true;
excel.boldFont(Excel,sheetIDX,boldIDX);

% Blue font for sums
color = 'bright blue';
colorIDX = false(m,n);
colorIDX(7:end,2+numSites) = true;
colorIDX(7:end,4+numSites*2) = true;
colorIDX(end,:) = true;
excel.fontColor(Excel,sheetIDX,colorIDX,color);

% Save open worksheet
excel.close(Excel,Workbook)




