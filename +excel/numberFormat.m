function numberFormat(Excel,sheetIDX,cellIDX,format)
% Applies number format to indexed cells
%
%   Inputs
%       Excel: Excel application object
%       sheetIDX: Excel worksheet index
%       cellIDX: index indicating which cells are to be formatted (<mxn>logical)
%       format: format to be applied (string, eg, "0.00")
  
    Worksheets = get(Excel,'Worksheets');
    Worksheet = Worksheets.Item(sheetIDX);    
    [m,n] = find(cellIDX);
    assert(~isempty(m),'No true cells in cellIDX')
    cellRef = excel.cellRef([m,n]);
    numCells = length(m);
    first = 1;
    last = min(numCells,40);
    while first <= numCells
        cellRefStr = [sprintf('%s,',cellRef{first:last}),cellRef{last}];
        Range = get(Worksheet,'Range',cellRefStr);
        Range.NumberFormat = format;
        first = last + 1;
        last = min(numCells,first + 40);
    end