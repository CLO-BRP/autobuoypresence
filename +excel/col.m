function a = col(n)
% find Excel column letter
%   Input
%       n = column index
%   Output
%       a = column letters

    a = char(mod(n-1,26)+65);
    if n>26
        a = [char(floor((n-1)/26)+64),a];
    end
    