function alignCell(Excel,sheetIDX,cellIDX,textAlignStr)
% Aligns indexed cells horizontally
%
%   Inputs
%       Excel: Excel application object
%       sheetIDX: Excel application object
%       sheetIDX: Excel worksheet index
%       cellIDX: index indicating which cells are to be centered (<mxn>logical)
%       textAlignStr: horizontal alignment format (string: "left", "center", "right")
%
%   Note: Loop is required because "get(Worksheet,'Range'" is limited to
%   a string argument of 255 characters are less.

    switch textAlignStr
        case "left"
            textAlign = 1;
        case "center"
            textAlign = 3; %What's the difference compared to -4108 ?!?!
        case "right"
            textAlign = 4;
    end
            
    Worksheets = get(Excel,'Worksheets');
    Worksheet = Worksheets.Item(sheetIDX);    
    [m,n] = find(cellIDX);
    cellRef = excel.cellRef([m,n]);
    numCells = length(m);
    first = 1;
    last = 40;
    while first <= numCells
        cellRefStr = [sprintf('%s,',cellRef{first:last}),cellRef{last}];
        Range = get(Worksheet,'Range',cellRefStr);
        Range.HorizontalAlignment = textAlign;        
        first = last + 1;
        last = min(numCells,first + 40);
    end
    
    
    %-------------------------------------------------------------------------------------------------------
% %   % Selects all cells in the current worksheet in the specified range.
% %   % Horizontally aligns all the specified cell range.
% %   % horizAlign = 1 for left alignment.
% %   % horizAlign = 3 for center alignment.
% %   % horizAlign = 4 for right alignment.
% %   % Leaves with cell A1 selected.
% %   function AlignCells(Excel, cellReference, horizAlign, autoFit)
% %     try
% %       % Select the range
% %       Excel.Range(cellReference).Select;
% %       % Align the cell contents.
% %       Excel.Selection.HorizontalAlignment = horizAlign;
% %       Excel.Selection.VerticalAlignment = 2;
% %       if autoFit
% %         % Auto fit all the columns.
% %         Excel.Cells.EntireColumn.AutoFit;
% %       end
% %       % Put "cursor" or active cell at A1, the upper left cell.
% %       Excel.Range('A1').Select;
% %     catch ME
% %       errorMessage = sprintf('Error in function AlignCells.\nError Message:\n%s', ME.message);
% %       fprintf('%s\n', errorMessage);
% %       WarnUser(errorMessage);
% %     end % from AlignCells
% %     return;
% %   end